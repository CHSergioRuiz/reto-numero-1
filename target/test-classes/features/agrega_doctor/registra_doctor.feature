#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@regresion
Feature: Realizar el Registro de un Doctor
  I want to use this template for my feature file

  @agregaDoctor
  Scenario: Realizar el Registro de un Doctor
    Given Ingresar a la Página del Sistema de Administración de Hospitales
    When Ingresa a la opcion Agregar Doctor
    Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente

