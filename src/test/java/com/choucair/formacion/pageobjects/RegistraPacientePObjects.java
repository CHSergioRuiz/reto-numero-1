package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://automatizacion.herokuapp.com/pperez/")
public class RegistraPacientePObjects extends PageObject
{

	public void datosPaciente(String nombre, String apellido, String telefono, String documento) {
		findBy("//A[@href='addPatient']").click();
		findBy("(//INPUT[@type='text'])[1]").type(nombre);
		findBy("(//INPUT[@type='text'])[2]").type(apellido);
		findBy("(//INPUT[@type='text'])[3]").type(telefono);
		findBy("//SELECT[@class='form-control']/ option [1]").click();
		findBy("(//INPUT[@type='text'])[4]").type(documento);
		findBy("//INPUT[@type='checkbox']").click();
		findBy("//A[@onclick='submitForm()'][text()='Guardar']").click();		
	}
	
}
