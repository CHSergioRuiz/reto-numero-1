package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://automatizacion.herokuapp.com/pperez/") 
public class RegistraDoctorPObjects extends PageObject{

	public void datosDoctor(String nombre, String apellido, String telefono, String documento) 
	{
		// TODO Auto-generated method stub
		
		findBy("//A[@href='addDoctor']").click();
		findBy("//INPUT[@id='name']").type(nombre);
		findBy("//INPUT[@id='last_name']").type(apellido);
		findBy("//INPUT[@id='telephone']").type(telefono);
		findBy("//select[@id='identification_type']/ option [1]").click();
		findBy("//INPUT[@id='identification']").type(documento);
		findBy("//A[@onclick='submitForm()'][text()='Guardar']").click();
		
	}			

}
