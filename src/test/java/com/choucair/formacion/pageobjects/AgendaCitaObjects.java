package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://automatizacion.herokuapp.com/pperez/") 
public class AgendaCitaObjects extends PageObject
{

	public void datosCita(String diaDeLaCita, String docIdPaciente, String docIdDoctor, String observaciones) 
	{
		// TODO Auto-generated method stub
		findBy("//A[@href='appointmentScheduling']").click();
		findBy("//INPUT[@id='datepicker']").type(diaDeLaCita);
		findBy("(//INPUT[@type='text'])[2]").type(docIdPaciente);
		findBy("(//INPUT[@type='text'])[3]").type(docIdDoctor);
		findBy("//TEXTAREA[@class='form-control']").type(observaciones);
		findBy("//A[@onclick='submitForm()'][text()='Guardar']").click();
			
	}

	
}
