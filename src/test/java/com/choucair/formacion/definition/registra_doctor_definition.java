package com.choucair.formacion.definition;

import com.choucair.formacion.steps.RegistraDoctorSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;


public class registra_doctor_definition 
{
	@Steps
	RegistraDoctorSteps registraDoctorSteps;
	
	@Given("^Ingresar a la Página del Sistema de Administración de Hospitales$")
	public void ingresar_a_la_Página_del_Sistema_de_Administración_de_Hospitales()  
	{
		registraDoctorSteps.acceso_Url();	
	}
	
	@When("^Ingresa a la opcion Agregar Doctor$")
	public void ingresa_a_la_opcion_Agregar_Doctor()
	{
		registraDoctorSteps.ingreso_doctor();
	}
	
	@Then("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
	public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente()
	{
		registraDoctorSteps.verificacion();
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			
		}
		
	}
}
