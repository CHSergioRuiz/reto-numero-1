package com.choucair.formacion.definition;



import com.choucair.formacion.steps.AgendaCitaSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class AgendaCitaDefinition 
{
	AgendaCitaSteps agendaCitaSteps;
	
	@Given("^Realiza el agendamiento de una Cita$")
	public void realiza_el_agendamiento_de_una_Cita() 
	{
		agendaCitaSteps.accesoPagina();
	}

	@Then("^Verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
	public void verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente() 
	{
		agendaCitaSteps.agendaCita();
		
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			
		}
	}
}
