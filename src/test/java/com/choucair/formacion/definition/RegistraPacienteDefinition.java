package com.choucair.formacion.definition;

import com.choucair.formacion.steps.RegistraPacienteSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class RegistraPacienteDefinition 
{
	@Steps
	RegistraPacienteSteps registraPacienteSteps;
	
	@Given("^Realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void realiza_el_registro_del_mismo_en_el_aplicativo_de_Administración_de_Hospitales()
	{
		registraPacienteSteps.acceso_Url();
	}

	@Then("^verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
	public void verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente()
	{
		registraPacienteSteps.ingreso_paciente();
		 
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			
		}
	}
}
