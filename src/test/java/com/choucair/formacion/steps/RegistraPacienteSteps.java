package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.RegistraPacientePObjects;

import net.thucydides.core.annotations.Step;

public class RegistraPacienteSteps 
{

	RegistraPacientePObjects registraPacientePObjects;
	@Step
	public void acceso_Url()
	{
		registraPacientePObjects.open();
	}
	
	public void ingreso_paciente() 
	{
		// TODO Auto-generated method stub
		String nombre = "Armando";
		String apellido = "Becerra";
		String telefono = "3262930";
		String documento = "1026929123";
		registraPacientePObjects.datosPaciente( nombre,  apellido,  telefono,   documento);
	}
	
	
}
