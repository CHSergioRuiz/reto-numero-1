package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.AgendaCitaObjects;

import net.serenitybdd.core.pages.PageObject;

public class AgendaCitaSteps extends PageObject 
{
	
	AgendaCitaObjects	agendaCitaObjects;
	
	public void accesoPagina()
	{
		agendaCitaObjects.open();
	}
	
	public void agendaCita()
	{
		String diaDeLaCita = "03/14/2019";
		String docIdPaciente = "1037929123";
		String docIdDoctor = "1037929122";
		String observaciones = "Agendamiento cita de prueba automatizada";
		agendaCitaObjects.datosCita(diaDeLaCita, docIdPaciente, docIdDoctor, observaciones);
		
	}

}
