package com.choucair.formacion;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
//@CucumberOptions (features = "src/test/resources/features/", tags = "@SmokeTest")
//@CucumberOptions (features = "src/test/resources/features/agrega_doctor/registra_doctor.feature", tags = "@agregaDoctor")
//@CucumberOptions (features = "src/test/resources/features/Registra_paciente/registra_Paciente.feature", tags = "@registraPaciente")
@CucumberOptions (features = "src/test/resources/features/Agenda_Cita/AgendaCita.feature", tags = "@AgendaCita")
public class RunnerTags {

}
